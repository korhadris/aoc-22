AOC 2022 Retrospectives

# Day 01

I should wrap the top section with conditional execution to make it
easier to type in the REPL.

```lisp
; Wrap this stuff
(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)
```

## New functions

* Used `before` in `loop`
* Added `str` library


# Day 02

I could go back and replace the `#\A` bits with `:rock`, but I'm glad
that I didn't try to do something similar with the parsing of X/Y/Z,
due to the changes between part one and part two.


# Day 03

I got bit by over-parsing for part one. I went back and only parsed
out the line, whereas originally, I was parsing the line into the two
compartments. I tried to work with the compartments for part two, but
it really messed with the recursion for `matching-items`. I was about
to just write `matching-items` without recursion, just looping over
three elements of the group, but I wanted to make something recursive.


## New stuff
* (sorta new) Using `|a|` to get the lower-case variable name. I got
  bit by the upcasing during READ.
* (removed) `str:join` worked as expected, but I took it out when I
  stopped trying to join the compartments back together.

## Revisions
* Went back and replaced `|a|` with `lower-a` and `A` with `upper-a`.


# Day 04

This was very straightforward. I went back and replaced initial
parsing of the tasks to a list of four values, versus two lists of two
values. This made later use of `destructuring-bind` easier to use when
processing the data.


# Day 05

Parsing was a bit more ugly than I wanted. Lots of reversing lines to
get the order of each stack the way I wanted. The rest went pretty
smoothly. I could definitely refactor some repeated code between part
one and part two.

## New stuff
* (sorta) Had to look up `multiple-value-bind`. Long macro names with
  `destructuring-bind`. I'd make my own macro to wrap them if it
  probably wasn't bad CL practice to hide common macros / names.


# Day 06

Quickest so far. I could refactor part one and part two into a
function that takes the number of characters to use in comparisons. I
use `loop` a lot in my CL, but it's supposed to be fast, and I haven't
had any performance issues with anything so far.

## New stuff
* (sorta) Used `finally` in a `loop` again. Had to look up the nested `return`
  part of it.
  
  
