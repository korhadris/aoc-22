(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-data (filename)
  (with-open-file (stream filename)
    (read-line stream nil nil)))

(defun unique-symbols (symbols)
  (loop for symbol across symbols
        when (< 1 (count symbol symbols))
          return nil
        finally (return t)))

(defun part-one (data)
  (loop for ii from 4 to (length data)
        when (unique-symbols (subseq data (- ii 4) ii))
          return ii))

(defun part-two (data)
  (loop for ii from 14 to (length data)
        when (unique-symbols (subseq data (- ii 14) ii))
          return ii))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-06.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-06.data")))
