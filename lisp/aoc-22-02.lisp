(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (list (aref line 0) (aref line 2)))

(defun parse-data (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil nil)
          while line
          collect (parse-line line))))

(defun round-one-score (round)
  (destructuring-bind (other choice) round
    (case choice
      (#\X ; Rock
       (1+ (ecase other
             (#\A 3)
             (#\B 0)
             (#\C 6))))
      (#\Y ; Paper
       (+ 2 (ecase other
             (#\A 6)
             (#\B 3)
             (#\C 0))))
      (#\Z ; Scissors
       (+ 3 (ecase other
             (#\A 0)
             (#\B 6)
             (#\C 3)))))))

(defun round-two-score (round)
  (destructuring-bind (other strategy) round
    (round-one-score
     (list other
           (case strategy
             (#\X ; Lose
              (ecase other
                (#\A #\Z)
                (#\B #\X)
                (#\C #\Y)))
             (#\Y ; Draw
              (ecase other
                (#\A #\X)
                (#\B #\Y)
                (#\C #\Z)))
             (#\Z ; Win
              (ecase other
                (#\A #\Y)
                (#\B #\Z)
                (#\C #\X))))))))

(defun part-one (data)
  (loop for round in data
        sum (round-one-score round)))

(defun part-two (data)
  (loop for round in data
        sum (round-two-score round)))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))
