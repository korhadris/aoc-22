(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  line)

(defun parse-data (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil nil)
          while line
          collect (parse-line line))))

(defun matching-item (rucksack)
  (let* ((compartment-size (/ (length rucksack) 2))
         (compartment-one (subseq rucksack 0 compartment-size))
         (compartment-two (subseq rucksack compartment-size)))
    (loop for item across compartment-one
          when (find item compartment-two)
            return item)))

(defun matching-items (sacks)
  (let ((first-sack (first sacks))
        (second-sack (second sacks)))
    (if second-sack
        (matching-items
         (cons (concatenate 'string (loop for item across first-sack
                                          when (find item second-sack)
                                            collect item))
               (cddr sacks)))
        (elt first-sack 0))))

(defun item-priority (item)
  (let ((code (char-code item))
        (lower-a (char-code #\a))
        (lower-z (char-code #\z))
        (upper-a (char-code #\A))
        (upper-z (char-code #\Z)))
    (cond
      ((<= lower-a code lower-z)
       (+ 1 (- code lower-a)))
      ((<= upper-a code upper-z)
       (+ 27 (- code upper-a))))))

(defun part-one (data)
  (loop for sack in data
        sum (item-priority (matching-item sack))))

(defun part-two (data)
  (loop for ii from 0 to (1- (length data)) by 3
        sum (item-priority (matching-items (subseq data ii (+ 3 ii))))))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))
