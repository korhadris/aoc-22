(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (loop for val in (cl-ppcre:split "[,-]" line)
        collect (parse-integer val)))

(defun parse-data (filename)
  (with-open-file (stream filename)
    (loop for line = (read-line stream nil nil)
          while line
          collect (parse-line line))))

(defun overlaps-all (tasks)
  (destructuring-bind (start-one end-one start-two end-two) tasks
    (or (and (<= start-one start-two)
             (<= end-two end-one))
        (and (<= start-two start-one)
             (<= end-one end-two)))))

(defun overlaps-part (tasks)
  (destructuring-bind (start-one end-one start-two end-two) tasks
    (or (<= start-one start-two end-one)
        (<= start-two start-one end-two))))

(defun part-one (data)
  (loop for tasks in data
        when (overlaps-all tasks)
          sum 1))

(defun part-two (data)
  (loop for tasks in data
        when (overlaps-part tasks)
          sum 1))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-04.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-04.data")))
