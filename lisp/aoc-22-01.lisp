(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (unless (str:emptyp line)
    (parse-integer line)))

(defun parse-data (filename)
  (with-open-file (stream filename)
    (let (data
          elf)
      (loop for line = (read-line stream nil nil)
            while line
            do (if (str:emptyp line)
                   (progn
                     (setf data (cons elf data))
                     (setf elf nil))
                   (setf elf (cons (parse-integer line) elf)))
            finally (setf data (cons elf data)))
      data)))

(defun part-one (data)
  (first
   (sort
    (loop for elf in data
          collect (apply #'+ elf))
    #'>)))

(defun part-two (data)
  (let ((calories 
          (sort
           (loop for elf in data
                 collect (apply #'+ elf))
           #'>)))
    (loop for ii below 3
          for elf in calories
          sum elf)))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))
