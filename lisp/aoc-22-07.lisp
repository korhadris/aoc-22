(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (when line
    ;; (format t "~A~%" line)
    (let ((split (str:split " " line)))
      (case (elt line 0)
        (#\$ (cons :command (cdr split)))
        (#\d (list :dir (second split)))
        (t (list :file (parse-integer (first split)) (second split)))))))

(defun change-path (old-path new-path)
  (cond
    ((equal "/" new-path) new-path)
    ((equal ".." new-path) (cl-ppcre:regex-replace "(.*/).*/" old-path "\\1"))
    (t (format nil "~a~a/" old-path new-path))))

(defun parse-data (filename)
  (let ((path "/")
        dirs
        files)
    (with-open-file (stream filename)
      (loop for command = (parse-line (read-line stream nil nil))
            while command
            do (cond
                 ((and (eq :command (first command))
                       (equal "cd" (second command)))
                  (setf path (change-path path (third command)))
                  (setf dirs (adjoin path dirs :test #'equal)))
                 ((eq :file (first command))
                  (setf files (cons (cons (format nil "~a~a" path (third command))
                                          (second command))
                                    files)))))
      (list dirs files))))

(defun directory-sizes (data)
  (loop for dir in (first data)
        collect (loop for file in (second data)
                      when (str:starts-with? dir (car file))
                        sum (cdr file))))

(defun part-one (data)
  (loop for dir in (directory-sizes data)
        when (<= dir 100000)
          sum dir))

(defun part-two (data)
  (let* ((dir-sizes (directory-sizes data))
         (total-size (apply #'max dir-sizes))
         (space-needed (- total-size 40000000)))
    (loop for dir in dir-sizes
          when (<= space-needed dir)
            minimize dir)))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-07.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-07.data")))
