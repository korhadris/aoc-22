(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-initial-state (stream)
  (let ((stack-lines
          (loop for line = (read-line stream nil nil)
                while (find #\[ line)
                collect (loop for ii from 1 to (length line) by 4
                              collect (unless (equal #\space (elt line ii))
                                        (elt line ii))))))
    (read-line stream nil nil) ; discard empty line
    (let ((initial-state
            (make-array (length (first stack-lines)) :initial-element nil)))
      (loop for line in (nreverse stack-lines)
            do (loop for item in line
                     for ii from 0
                     when item
                       do (setf (aref initial-state ii)
                                (cons item (aref initial-state ii)))))
      initial-state)))

(defun parse-line (line)
  (loop for num in (cl-ppcre:all-matches-as-strings "[0-9]+" line)
        for index from 0
        collect (if (< 0 index)
                      (1- (parse-integer num)) ; Change to zero-based
                      (parse-integer num))))

(defun parse-moves (stream)
  (loop for line = (read-line stream nil nil)
        while line
        collect (parse-line line)))

(defun parse-data (filename)
  (with-open-file (stream filename)
    (cons (parse-initial-state stream)
          (parse-moves stream))))

(defun move-items (from to &optional (num 1))
  (let ((moved (loop for ii below num
                     for item in from
                     collect item)))
    (values (nthcdr num from)
            (append moved to))))

(defun move-item-one (state move)
  (destructuring-bind (num from to) move
    (loop for ii below num
          do (multiple-value-bind (new-from new-to)
                 (move-items (aref state from) (aref state to))
               (setf (aref state from) new-from)
               (setf (aref state to) new-to)))))

(defun move-item-two (state move)
  (destructuring-bind (num from to) move
    (multiple-value-bind (new-from new-to)
        (move-items (aref state from) (aref state to) num)
      (setf (aref state from) new-from)
      (setf (aref state to) new-to))))

(defun part-one (data)
  (let ((state (copy-seq (first data)))
        (moves (rest data)))
    (loop for move in moves
          do (move-item-one state move))
    (concatenate 'string
                 (loop for stack across state
                       collect (first stack)))))

(defun part-two (data)
  (let ((state (copy-seq (first data)))
        (moves (rest data)))
    (loop for move in moves
          do (move-item-two state move))
    (concatenate 'string
                 (loop for stack across state
                       collect (first stack)))))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-05.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-05.data")))
