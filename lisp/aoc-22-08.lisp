(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (let ((zero (char-code #\0)))
    (loop for char across line
          collect (- (char-code char) zero))))

(defun parse-data (filename)
  (with-open-file (stream filename)
    (let ((lines (loop for line = (read-line stream nil nil)
                       while line
                       collect (parse-line line))))
      (make-array (list (length lines) (length (first lines)))
                  :initial-contents lines))))

(defun check-visibility (data)
  (let ((num-rows (first (array-dimensions data)))
        (num-cols (second (array-dimensions data)))
        (visibility (make-array (array-dimensions data) :initial-element nil)))
    (loop for row below num-rows
          ;; From left
          do (loop with highest = -1
                   for col below num-cols
                   until (= highest 9)
                   when (< highest (aref data row col))
                     do (progn
                          (setf (aref visibility row col) t)
                          (setf highest (aref data row col))))
          ;; From right
          do (loop with highest = -1
                   for col from (1- num-cols) downto 0
                   until (= highest 9)
                   when (< highest (aref data row col))
                     do (progn
                          (setf (aref visibility row col) t)
                          (setf highest (aref data row col)))))
    (loop for col below num-cols
          ;; From top
          do (loop with highest = -1
                   for row below num-rows
                   until (= highest 9)
                   when (< highest (aref data row col))
                     do (progn
                          (setf (aref visibility row col) t)
                          (setf highest (aref data row col))))
          ;; From bottom
          do (loop with highest = -1
                   for row from (1- num-rows) downto 0
                   until (= highest 9)
                   when (< highest (aref data row col))
                     do (progn
                          (setf (aref visibility row col) t)
                          (setf highest (aref data row col)))))
    visibility))

(defun house-score (data row col)
  (let ((start (aref data row col))
        (num-rows (first (array-dimensions data)))
        (num-cols (second (array-dimensions data))))
    (*
     ;; Looking up
     (loop for other from (1- row) downto 0
           sum 1
           until (<= start (aref data other col)))
     ;; Looking left
     (loop for other from (1- col) downto 0
           sum 1
           until (<= start (aref data row other)))
     ;; Looking down
     (loop for other from (1+ row) below num-rows
           sum 1
           until (<= start (aref data other col)))
     ;; Looking right
     (loop for other from (1+ col) below num-cols
           sum 1
           until (<= start (aref data row other))))))

(defun count-visible (visibility)
  (destructuring-bind (num-rows num-cols) (array-dimensions visibility)
    (loop for row below num-rows
          sum (loop for col below num-cols
                    when (aref visibility row col)
                      sum 1))))

(defun part-one (data)
  (count-visible (check-visibility data)))

(defun part-two (data)
  (destructuring-bind (num-rows num-cols) (array-dimensions data)
    (loop for row from 1 below (1- num-rows)
          maximize (loop for col from 1 below (1- num-cols)
                         maximize (house-score data row col)))))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-08.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-08.data")))
