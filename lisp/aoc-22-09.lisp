(ql:quickload '(:split-sequence :cl-ppcre :str) :silent t)

(defpackage :aoc-22
  (:use :cl)
  (:export :toplevel :parse-data :part-one :part-two))

(in-package :aoc-22)

(defun parse-line (line)
  (let ((split (str:split " " line)))
    (list (ecase (elt (first split) 0)
            (#\D :down)
            (#\L :left)
            (#\R :right)
            (#\U :up))
          (parse-integer (second split)))))

(defun parse-data (filename)
  (with-open-file (stream filename)
    
    (loop for line = (read-line stream nil nil)
          while line
          collect (parse-line line))))

(defun move-head (head move)
  (destructuring-bind (pos-x pos-y) head
    (destructuring-bind (direction distance) move
      (ecase direction
        (:down (list pos-x (- pos-y distance)))
        (:left (list (- pos-x distance) pos-y))
        (:right (list (+ pos-x distance) pos-y))
        (:up (list pos-x (+ pos-y distance)))))))

(defun sign (val)
  (cond
    ((< 0 val) 1)
    ((> 0 val) -1)
    (t 0)))

(defun move-tail (head tail)
  (destructuring-bind (head-x head-y) head
    (destructuring-bind (tail-x tail-y) tail
      (let ((diff-x (- head-x tail-x))
            (diff-y (- head-y tail-y)))
        (cond
          ((< 1 (abs diff-x))
           (list (+ (sign diff-x) tail-x)
                 (if (= 0 diff-y)
                     tail-y
                     (+ (sign diff-y) tail-y))))
          ((< 1 (abs diff-y))
           (list (if (= 0 diff-x)
                     tail-x
                     (+ (sign diff-x) tail-x))
                 (+ (sign diff-y) tail-y)))
          (t tail))))))

(defun follow-head (head tail)
  (loop with last-tail = tail
        with next-tail = (move-tail head tail)
        until (equal last-tail next-tail)
        collect next-tail
        do (setf last-tail next-tail)
        do (setf next-tail (move-tail head next-tail))))

(defun update-path (head tail tail-path path)
  ;; (format t "~A ~A~%" head tail)
  (if (null path)
      (list head tail tail-path)
      (let* ((next-head (move-head head (first path)))
             (next-tails (follow-head next-head tail)))
        (update-path next-head
                     (if next-tails (first (last next-tails)) tail)
                     (append next-tails tail-path)
                     (rest path)))))

(defun rope-path (rope tail-path paths)
  (if (null paths)
      tail-path
      (loop with next-head = (move-head (first rope) (first paths))
            for part in 1 below (length rope)
            when (= part (1- (length rope)))
              append 
            do (update-path (aref rope (1- part)) (aref rope part) nil (list (first path))))
      ))

(defun part-one (data)
  (destructuring-bind (head tail tail-path) (update-path '(0 0) '(0 0) '((0 0)) data)
    (length (remove-duplicates tail-path :test #'equal))))

(defun part-two (data)
  (length (remove-duplicates (rope-path (make-array 10 :initial-element '(0 0)) '(0) data)))
  (destructuring-bind (num-rows num-cols) (array-dimensions data)
    (loop for row from 1 below (1- num-rows)
          maximize (loop for col from 1 below (1- num-cols)
                         maximize (house-score data row col)))))

(defun toplevel ()
  (let* ((filename (second sb-ext:*posix-argv*))
         (data (parse-data filename)))
    (format t "Part one: ~a~%" (part-one data))
    (format t "Part two: ~a~%" (part-two data))))

(defun test-one ()
  (part-one (parse-data "../data/aoc-22-09.data")))

(defun test-two ()
  (part-two (parse-data "../data/aoc-22-09.data")))
