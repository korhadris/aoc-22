THIS_FILE := $(lastword $(MAKEFILE_LIST))

files := $(wildcard lisp/*.lisp)
names := $(patsubst lisp/%.lisp,%,$(files))
tests := $(patsubst aoc-22-%,test-%,$(names))

.PHONY: all
all: $(names)

.PHONY: print
print:
	@echo $(data)

.PHONY: $(names)
$(names): %: bin/%

bin/%: lisp/%.lisp build-bin.sh
	@mkdir -p bin
	@echo "Compiling $< to $@"
	@build-bin.sh $< $@

.PHONY: clean
clean:
	@rm -frv bin

.PHONY: test $(tests)
test: $(names) $(tests)

data/aoc-22-%.data:
	@test -n "${AOC_ID}" || (echo '$$AOC_ID is unset' 1>&2 ; false)
	@curl https://adventofcode.com/2022/day/$$(echo $@ | sed 's,.*-0*\([0-9]*\)\.data,\1,')/input \
    -b "session=${AOC_ID}" -o $@

$(tests): test-%: bin/aoc-22-% data/aoc-22-%.data
	$^

.PHONY: list
list:
	@LC_ALL=C $(MAKE) -pRrq -f $(THIS_FILE) : 2>/dev/null \
    | awk -v RS= -F: \
    '/(^|\n)# Files(\n|$$)/,/(^|\n)# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' \
    | sort | egrep -v -e '^[^[:alnum:]]'
