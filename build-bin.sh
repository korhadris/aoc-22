#!/usr/bin/env bash

set -euo pipefail

INPUT=$1
OUTPUT=$2
NAME=$(grep '(defpackage' $INPUT | sed "s/.*defpackage *[:']\([^ )]*\).*/\1/")

sbcl --noinform --disable-debugger \
     --load "$INPUT" \
     --eval "(sb-ext:save-lisp-and-die \"$OUTPUT\"
               :executable t
               :save-runtime-options t
               :toplevel '$NAME:toplevel)"
